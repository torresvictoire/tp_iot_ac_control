# aws_iot_certificate cert
resource "aws_iot_certificate" "cert" {
  active = true
}


# aws_iot_policy pub-sub
resource "aws_iot_policy" "pubsub" {
  name = "PubSubToAnyTopic"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = file("files/iot_policy.json")
}
# aws_iot_policy_attachment attachment
resource "aws_iot_policy_attachment" "att" {
  policy = aws_iot_policy.pubsub.name
  target = aws_iot_certificate.cert.arn
}
# aws_iot_thing temp_sensor
resource "aws_iot_thing" "temp_sensor" {
  name = "temp_sensor"
  attributes = {
    First = "examplevalue"
  }
}
# aws_iot_thing_principal_attachment thing_attachment
resource "aws_iot_thing_principal_attachment" "att" {
  principal = aws_iot_certificate.cert.arn
  thing     = aws_iot_thing.temp_sensor.name
}
# data aws_iot_endpoint to retrieve the endpoint to call in simulation.py
data "aws_iot_endpoint" "iot_endpoint" {
  endpoint_type = "iot:Data-ATS"
}



# aws_iot_topic_rule rule for sending invalid data to DynamoDB
resource "aws_iot_topic_rule" "rule" {
  name        = "MyRule"
  description = "Example rule"
  enabled     = true
  sql         = "SELECT *  FROM 'sensor/temperature/+' where temperature >= 40"
  sql_version = "2016-03-23"
  dynamodbv2 {
    put_item {
      table_name = "Temperature"
    }
    role_arn = aws_iam_role.iot_role.arn
  }
}
# aws_iot_topic_rule rule for sending valid data to Timestream
resource "aws_iot_topic_rule" "rule2" {
  name        = "MyRule2"
  description = "Example rule"
  enabled     = true
  sql         = "SELECT * FROM 'topic/test'"
  sql_version = "2016-03-23"
  timestream {
    database_name = "iot"
    dimension {
      name  = "sensor_id"
      value = "$${sensor_id}"
    }
    dimension {
      name  = "temperature"
      value = "$${temperature}"
    }
    dimension {
      name  = "zone_id"
      value = "$${zone_id}"
    }
    role_arn = aws_iam_role.iot_role.arn
    table_name = "temperaturesensor"
    timestamp {
      unit  = "MILLISECONDS"
      value = "$${timestamp()}"
    }
  }
}

###########################################################################################
# Enable the following resource to enable logging for IoT Core (helps debug)
###########################################################################################

#resource "aws_iot_logging_options" "logging_option" {
#  default_log_level = "WARN"
#  role_arn          = aws_iam_role.iot_role.arn
#}
