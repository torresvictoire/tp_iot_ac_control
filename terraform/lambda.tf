# aws_lambda_function to control air conditioner

resource "aws_lambda_function" "ac_control_lambda" {
  # If the file is not in the current working directory you will need to include a
  # path.module in the filename.
  filename      = "files/empty_package.zip"
  function_name = "ac_control_lambda"
  role          = aws_iam_role.lambda_role.arn
  handler       = "ac_control_lambda.lambda_handler"


  runtime = "python3.7"
}
# aws_cloudwatch_event_rule scheduled action every minute
resource "aws_cloudwatch_event_rule" "every_one_minute" {
  schedule_expression = "rate(1 minute)"
}
# aws_cloudwatch_event_target to link the schedule event and the lambda function
resource "aws_cloudwatch_event_target" "lambda" {
  target_id = "lambda"
  rule      = aws_cloudwatch_event_rule.every_one_minute.name
  arn       = aws_lambda_function.ac_control_lambda.arn

}

# aws_lambda_permission to allow CloudWatch (event) to call the lambda function
resource "aws_lambda_permission" "allow_cloudwatch" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.ac_control_lambda.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.every_one_minute.arn

}